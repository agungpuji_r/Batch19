console.log('=============================')
//Soal No. 1 (Range)
function range(startNum, finishNum) {
    var arr= [];
    if (startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i++) {
            arr.push(i);
        } 
    } else if (startNum > finishNum) {
        for (var i = startNum; i >= finishNum; i--) {
            arr.push(i);
        }
    } else {
        return -1;
    }
    return arr
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 


console.log('=============================')
// Soal No. 2(Range with Step)
function rangeWithStep(startNum, finishNum, step) {
    var arr= [];
    if (startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i += step) {
            arr.push(i);
        }
    } else if (startNum > finishNum) {
        for (var i = startNum; i >= finishNum; i -= step) {
            arr.push(i);
        }
    } else {
        return -1;
    }
    return arr
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 


console.log('=============================')
// Soal No. 3 (Sum of Range)
function sum(startNum, finishNum, step) {
    var arr= 0
    if (startNum < finishNum) {
        if (typeof step ==='number') {
            for (var i = startNum; i <= finishNum; i += step) {
                arr += (i);
            }
        } else {
            for (var i = startNum; i <= finishNum; i ++) {
                arr += (i);
            }
        }
    } else if (startNum > finishNum) {
        if (typeof step === 'number') {
            for (var i = startNum; i >= finishNum; i -= step) {
                arr += (i);
            }
        } else {
            for (var i = startNum; i >= finishNum; i --) {
                arr += (i);
            }
        }
    } else if (startNum) {
        return arr + startNum;
    } else {
        return arr;
    }
    return arr
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


console.log('=============================')
// Soal No. 4 (Array Multidimensi)
function dataHandling(input) {
    var data = '';
    for (var i = 0; i < input.length ; i++) {
        data += 'Nomer ID:  ' + input[i][0] + '\n'
        + 'Nama Lengkap:  ' + input[i][1] + '\n'
        + 'TTL:  ' + input[i].slice(2,4).join(' ') + '\n' 
        + 'Hobi: ' + input[i][4] + '\n' + '\n';
    }
    return console.log(data)
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
dataHandling(input)


console.log('=============================')
//Soal No. 5 (Balik Kata)
function balikKata(kata) {
    kataBaru = '';
    for (var i = kata.length - 1; i >= 0 ; i--) {
        kataBaru += kata[i];
    }
    return kataBaru;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


console.log('=============================')
//Soal No. 6 (Metode Array)
function dataHandling2(input2) {
    input2.splice(1, 1, 'Roman Alamsyah Elsharawy');
    input2.splice(2, 1, 'Provinsi Bandar Lampung')
    input2.splice(4, 1, 'Pria', 'SMA Internasional Metro');
    console.log(input2);
    var tanggal = input2[3].split('/');
    switch (tanggal[1]) {
        case '01':
            console.log('Januari');
            break;
        case '02':
            console.log('Februari');
            break;
        case '03':
            console.log('Maret');
            break;
        case '04':
            console.log('April');
            break;
        case '05':
            console.log('Mei');
            break;
        case '06':
            console.log('Juni');
            break;
        case '07':
            console.log('Juli');
            break;
        case '08':
            console.log('Agustus');
            break;
        case '09':
            console.log('September');
            break;
        case '10':
            console.log('Oktober');
            break;
        case '11':
            console.log('November');
            break;
        case '12':
            console.log('Desember');
            break;
    };
    var urutTanggal = input2[3].split('/').sort(function(a, b){return b - a});
    console.log(urutTanggal);
    const gabungTanggal = tanggal.join('-');
    console.log(gabungTanggal);
    var nama = input2[1].slice(0, 14);
    console.log(nama);
}

var input2 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]  

 dataHandling2(input2)
 /**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 