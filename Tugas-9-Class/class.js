//Soal 1. Animal Class
//Release 0
class Animal {
    constructor(name, legs, cold_blooded) {
        this.aname = 'shaun'
        this.alegs = 4
        this.acold_blooded = false
    }
    get name() {
        return this.aname
    }
    get legs() {
        return this.alegs
    }
    get cold_blooded() {
        return this.acold_blooded
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// Release 1
class Ape extends Animal {
    constructor(name, legs, cold_blooded) {
        super(name, legs, cold_blooded)
        this.aname = 'kera sakti'
        this.alegs = 2
        this.acold_blooded = true
    }
    yell() {
        return console.log('Auooo')
    }
}
class Frog extends Animal {
    constructor(name, legs, cold_blooded) {
        super(name, legs, cold_blooded)
        this.aname = 'buduk'
        this.alegs = 4
        this.acold_blooded = false
    }
    jump() {
        console.log('hop hop')
    }
}
 
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

//Soal 2. Function to Class
class Clock {
    constructor({ template }) {
        this.template = template
    }
    render() {
        var date = new Date();
        
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
            
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
            
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
            
        var output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);
                
        console.log(output);
    }
        
    stop () {
        return clearInterval(this.timer);
    }

    start () {
    this.render ()
    this.timer = setInterval(() => this.render(), 1000);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();   