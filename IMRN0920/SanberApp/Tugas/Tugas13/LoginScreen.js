import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text,
    TextInput,
    TouchableOpacity
} from 'react-native';

export default class App extends Component {
    render() {
        return (
        <View style={styles.container}>
            <View style={styles.logo}>
                <Image source={require('./images/logo.png')} style={{ width: 300, height: 100 }} /></View> 
                <Text style={styles.paragraph}>Login</Text>
                <View style={styles.form}>
                    <Text>Username / Email</Text>
                    <TextInput 
                        style={{
                            height: 40,
                            borderColor: "black",
                            borderWidth: 0.5,
                            marginBottom: 5
                        }}
                    />
                    <Text>Password</Text>
                    <TextInput 
                        style={{
                            height: 40,
                            borderColor: "black",
                            borderWidth: 0.5
                    }}
                />
                <TouchableOpacity style={styles.button}>
                    <Text style={{color:'white', textAlign: 'center'}}>Masuk</Text>
                </TouchableOpacity>
            </View>
        </View>
        );
    }
}

const styles = StyleSheet.create({
    container : {
        flex: 1,
    },
    logo: {
      alignItems: 'center',
      marginTop: 50,
      marginBottom: 20
    },
    paragraph: {
      fontSize: 18,
      textAlign: 'center'
    },
    form: {
      padding: 50
    },
    button: {
      margin: 50,
      paddingTop:10,
      paddingBottom:10,
      textAlign:'center',
      borderRadius: 90,
      backgroundColor: '#3EC6FF',
      marginHorizontal: 100
    }
});