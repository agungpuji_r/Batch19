import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import YoutubeUi from './Tugas/Tugas12/App';
import Login from './Tugas/Tugas13/LoginScreen';
import About from './Tugas/Tugas13/AboutScreen';
import Tugas14 from './Tugas/Tugas14/App';
import TugasNavigation from './Tugas/TugasNavigation/index'
import Quiz3 from './Quiz3/index'

export default function App() {
  return (
    <Quiz3 />
    //<YoutubeUi />
    //<Login />
    //<About />
    //<Tugas14 />
    //<TugasNavigation />
    //<View style={styles.container}>
      //<Text>Open up App.js to start working on your app!</Text>
      //<StatusBar style="auto" />
    //</View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
