//No. 1 Looping While
var pertambahan = 2;
var pengurangan = 20;
console.log("LOOPING PERTAMA");
while (pertambahan <= 20) {
    console.log(pertambahan + ' - I love coding');
    pertambahan +=2;
}
console.log('LOOPING KEDUA');
while (pengurangan >= 2) {
    console.log(pengurangan + ' I will become a mobile developer');
    pengurangan -= 2;
}

console.log('\n');
//No. 2 Looping menggunakan for
var totalLoop = 20;

for (var x = 1; x <= totalLoop; x++) {
  if (x%2===0) {
    console.log(x + ' - Berkualitas');
  } else if (x%3===0) {
    console.log(x + ' - I love coding');
  } else {
    console.log(x + ' - Santai');
  }
} 

console.log('\n');
//No. 3 Membuat Persegi Panjang
var lebar = 4;
var panjang = 8;
var sharp = '#';
var i = 0;
var j = 0;

while (i < lebar) {
  while (j < panjang) {
    j++;
    sharp += '#';
  }
  console.log(sharp);
  sharp += ''
  i++
}


console.log('\n');
//No. 4 Membuat Tangga
var jumlahTangga = 7;
var tangga = '#';
var x = 0;
var y = 0;

while (x < jumlahTangga) {
    while (y < jumlahTangga) {
      y++;
      tangga += '';
    }
    console.log(tangga);
    tangga += '#';
    x++;
  }

console.log('\n');
//No. 5 Membuat Papan Catur
var lebar = 8;
var papan = '';
for (var t = 0; t < lebar; t++) { 
  for (var l = 0; l <= lebar; l++) {
    if(lebar - l === 0) {
      papan += "\n";
    } else if ((t + l)%2 === 0) {
      papan += " ";
    } else {
      papan += "#"
    } 
  }
}
console.log(papan)