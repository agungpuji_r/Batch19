// Soal No. 1 (Array to Object)
function arrayToObject(arr) {
    var now = new Date();
    var thisYear = now.getFullYear();
    var obj = {}; 
    var last =[]
    if (arr.length < 1 ) {
        return console.log('');
    } else {
        for (var i = 0; i < arr.length; i++) {
            console.log((i + 1) +'. ' + arr[i].slice(0, 2).join(' ') + ': ');
            obj.firstName = arr[i][0];
            obj.lastName = arr[i][1];
            obj.gender = arr[i][2];
            obj.age = thisYear - arr[i][3];
            if (arr[i][3] === undefined || thisYear < arr[i][3]) {
                obj.age = 'Invalid Birth Year'
            }console.log(obj)
        }
        
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

console.log('=========================')
//Soal No. 2 (Shopping Time)
function shoppingTime(memberId, money) {
    var data = {}
    var newId = {}
    var harga = [
    ['Sepatu Stacattu', 1500000],
    ['Baju Zoro', 500000],
    ['Baju H&N', 250000],
    ['Sweater Uniklooh', 175000],
    ['Casing Handphone', 50000]
    ];
    var namaBarang = [];
    if(memberId === '' || memberId === undefined) {
        return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    } else if (money < 50000) {
        return 'Mohon maaf, uang tidak cukup'}
    newId.memberId = memberId;
    newId.money = money;
    for (var i = 0; i < harga.length; i++) {
        if (money >= harga[i][1]) {
            money -= harga[i][1];
            namaBarang.push(harga[i][0])
        } 
    }
    newId.listPurchased = namaBarang
    newId.changeMoney = money
    return newId;
}
 
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log('=========================')
//Soal No. 3 (Naik Angkot)
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var totalTiket = []
    for (var i=0; i<arrPenumpang.length; i++) {
        var tiket = {}
        tiket.penumpang = arrPenumpang[i][0]
        tiket.naikDari = arrPenumpang[i][1]
        tiket.tujuan = arrPenumpang[i][2]
        tiket.bayar = Math.abs((arrPenumpang[i][1].charCodeAt(0) - arrPenumpang[i][2].charCodeAt(0)) * 2000)
        totalTiket.push(tiket)
    }
        return totalTiket
}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]